# импортируем нужные модули
from turtle import *
from random import randint

# размер игрового поля
width = 170
height = 200


# класс для спрайтов
class Sprite(Turtle):
    def __init__(self, x, y, step, shape, color):
        super().__init__()
        self.penup()  # поднимаем перо
        self.speed(0)  # скорость 0
        self.goto(x, y)  # перемещаем в начальную точку
        self.shape(shape)  # устанавливаем форму
        self.color(color)  # устанавливаем цвет
        self.step = step  # запоминаем шаг

    # движение вверх
    def move_up(self):
        self.goto(self.xcor(), self.ycor() + self.step)

    # движение вниз
    def move_down(self):
        self.goto(self.xcor(), self.ycor() - self.step)

    # движение влево
    def move_left(self):
        self.goto(self.xcor() - self.step, self.ycor())

    # движение вправо
    def move_right(self):
        self.goto(self.xcor() + self.step, self.ycor())

    # проверка столкновения с другим спрайтом
    def is_collide(self, sprite):
        dist = self.distance(
            sprite.xcor(), sprite.ycor()
        )  # ищем расстояние до другого спрайта
        # если расстояние меньше 30
        if dist < 30:
            return True  # столкнулись
        else:
            return False  # не столкнулись

    # задать направление движения
    def set_move(self, x_start, y_start, x_end, y_end):
        # запоминаем координаты начала движения
        self.x_start = x_start
        self.y_start = y_start
        # запоминаем координаты конца движения
        self.x_end = x_end
        self.y_end = y_end
        # перемещаемся на начало
        self.goto(x_start, y_start)
        # задаем направление движения к конечно точке
        self.setheading(self.towards(x_end, y_end))

    # сделать шаг
    def make_step(self):
        self.forward(self.step)  # двигаем спрайт на шаг
        # если расстояние от спрайта до точки конца движения МЕНЬШЕ длины шага
        if self.distance(self.x_end, self.y_end) < self.step:
            # меняем точки - начало теперь это конец, а конец - начало
            self.set_move(self.x_end, self.y_end, self.x_start, self.y_start)


# создаем нужные спрайты
player = Sprite(0, -100, 10, "circle", "orange")  # игрок
enemy1 = Sprite(-width, 0, 50, "square", "red")  # враг 1
enemy2 = Sprite(width, 70, 50, "square", "red")  # враг 2
goal = Sprite(0, 120, 0, "triangle", "green")  # цель

# задаем направление движения врагам
enemy1.set_move(-width, 0, width, 0)
enemy2.set_move(width, 70, -width, 70)

# объект Экран
scr = player.getscreen()
# активируем "прослушку" клавиш
scr.listen()
# привязываем клавиши к передвижению игрока
scr.onkey(player.move_up, "Up")
scr.onkey(player.move_down, "down")
scr.onkey(player.move_left, "left")
scr.onkey(player.move_right, "Right")

# счетчик касаний цели
total = 0

# пока не коснулись цели 3 раза
while total < 3:
    # шаг враго
    enemy1.make_step()
    enemy2.make_step()

    # если игрок коснулся цели
    if player.is_collide(goal):
        total += 1  # счетчик касаний +1
        player.goto(0, -100)  # возвращаем на начало игрока

    # если игрок коснулся врага 1 или 2
    if player.is_collide(enemy1) or player.is_collide(enemy2):
        goal.hideturtle()  # прячем цель
        break  # ломаем цикл

# если набрано 3 касания
if total == 3:
    # прячем врагов
    enemy1.hideturtle()
    enemy2.hideturtle()
    # выведем надпись о победе
    goal.goto(0, 0)
    goal.write("Ты победил!", font=("Arial", 15, "bold"))
# если не набрано 3 касания
else:
    # выведем надпись о проигрыше
    enemy1.goto(0, 0)
    enemy1.write("Ты проиграл!", font=("Arial", 15, "bold"))
    player.hideturtle()
